yum update -y

yum install -y nano mc php-curl php-fpm stunnel catdoc xpdf munin nagios sphinx

/* git */
rpm -Uvh http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-1.noarch.rpm
//rpm -Uvh http://opensource.wandisco.com/centos/6/git/x86_64/wandisco-git-release-6-1.noarch.rpm
yum install -y git


/* база данных */
#mkdir /var/lib/mysql/tmp
#chown mysql:mysql /var/lib/mysql/tmp
#chmod 775 -R /var/lib/mysql/tmp
#id mysql
#файл fstab добавляем запись uid и gid пользователя mysql
#tmpfs /var/lib/mysql/tmp tmpfs rw,gid=993,uid=995,size=1G,nr_inodes=10k,mode=0700 0 0
nano /usr/lib/systemd/system/mariadb.service
[Service]
LimitNOFILE=65000
/* база данных */


/* лимиты */
nano /etc/security/limits.conf
*   soft    nproc   65000
*   hard    nproc   1000000
*   -    nofile  1048576
root - memlock unlimited
/* лимиты */


/* сертификат */
#cd /usr/local/sbin
#sudo wget https://dl.eff.org/certbot-auto
#cd /tmp
#git clone https://github.com/certbot/certbot
#sudo chmod a+x /usr/local/sbin/certbot-auto
#certbot-auto certonly --webroot --agree-tos --email info@site.ru -w /home/bitrix/www/ -d site.ru -d www.site.ru -d test.site.ru
#openssl dhparam -out /etc/nginx/ssl/dhparam.pem 2048
#service nginx reload
#
#nano /etc/crontab
#>>добавляем строки
#30 2 * * 1 /usr/local/sbin/certbot-auto renew >> /var/log/le-renew.log
#35 2 * * 1 /etc/init.d/nginx reload
/* сертификат */


nano /usr/lib/systemd/system/mariadb.service
[Service]
LimitNOFILE=65000


#user 'west-tech' virtual host 'dev.west-tech.ru' configuration file
<VirtualHost 127.0.0.1:8080>
	ServerName dev.west-tech.ru
	AddDefaultCharset off
	AssignUserID west-tech west-tech
	DocumentRoot /var/www/west-tech/data/www/dev.west-tech.ru
	ServerAdmin webmaster@dev.west-tech.ru
	CustomLog /var/www/httpd-logs/dev.west-tech.ru.access.log combined
	ErrorLog /var/www/httpd-logs/dev.west-tech.ru.error.log
	ServerAlias www.dev.west-tech.ru
	DirectoryIndex index.html index.php
	SetEnvIf X-Forwarded-Proto https HTTPS=on
	<FilesMatch "\.ph(p[3-5]?|tml)$">
		SetHandler application/x-httpd-php
	</FilesMatch>
	<FilesMatch "\.phps$">
		SetHandler application/x-httpd-php-source
	</FilesMatch>
	<IfModule php5_module>
		php_admin_value sendmail_path "/usr/sbin/sendmail -t -i -f webmaster@dev.west-tech.ru"
		php_admin_value upload_tmp_dir "/var/www/west-tech/data/mod-tmp"
		php_admin_value session.save_path "/var/www/west-tech/data/mod-tmp"
		php_admin_value open_basedir "/var/www/west-tech/data:."
	</IfModule>
	<IfModule php7_module>
		php_admin_value sendmail_path "/usr/sbin/sendmail -t -i -f webmaster@dev.west-tech.ru"
		php_admin_value upload_tmp_dir "/var/www/west-tech/data/mod-tmp"
		php_admin_value session.save_path "/var/www/west-tech/data/mod-tmp"
		php_admin_value open_basedir "/var/www/west-tech/data:."
	</IfModule>
</VirtualHost>
<Directory /var/www/west-tech/data/www/dev.west-tech.ru>
	Options +Includes -ExecCGI
	<IfModule php5_module>
		php_admin_flag engine on
	</IfModule>
	<IfModule php7_module>
		php_admin_flag engine on
	</IfModule>
</Directory>

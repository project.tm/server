#user 'west-tech' virtual host 'old.west-tech.ru' configuration file
<VirtualHost 127.0.0.1:8080>
	ServerName old.west-tech.ru
	AddDefaultCharset off
	AssignUserID west-tech west-tech
	DocumentRoot /var/www/west-tech/data/www/west-tech.ru/old
	ServerAdmin webmaster@old.west-tech.ru
	CustomLog /var/www/httpd-logs/old.west-tech.ru.access.log combined
	ErrorLog /var/www/httpd-logs/old.west-tech.ru.error.log
	ServerAlias old2.west-tech.ru www.old2.west-tech.ru
	DirectoryIndex index.html index.php
	SetEnvIf X-Forwarded-Proto https HTTPS=on
	<FilesMatch "\.ph(p[3-5]?|tml)$">
		SetHandler application/x-httpd-php5
	</FilesMatch>
	ScriptAlias /php-bin/ /var/www/php-bin/west-tech/
	AddHandler application/x-httpd-php5 .php .php3 .php4 .php5 .phtml
	Action application/x-httpd-php5 /php-bin/php
</VirtualHost>
<Directory /var/www/west-tech/data/www/west-tech.ru/old>
	Options +Includes -ExecCGI
</Directory>
